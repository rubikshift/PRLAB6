package net.stawrul;

import net.stawrul.model.Book;
import net.stawrul.model.Order;
import net.stawrul.services.OrdersService;
import net.stawrul.services.exceptions.OutOfStockException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import java.math.BigDecimal;

import static junit.framework.TestCase.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class OrdersServiceTest
{
    @Mock
    EntityManager em;

    @Test
    public void whenOrderedBookAvailable_placeOrderDecreasesAmount()
    {
        //Arrange
        Order order = new Order();

        BigDecimal price = new BigDecimal(39.99);

        Book book = new Book();
        book.setAmount(3);
        book.setPrice(price);

        Book orderedBook = new Book();
        orderedBook.setAmount(2);

        order.getBooks().add(orderedBook);

        Mockito.when(em.find(Book.class, orderedBook.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert
        assertEquals(1, (int)book.getAmount());
    }

    @Test(expected = OutOfStockException.class)
    public void whenOrderedBookNotAvailable_placeOrderThrowsEx()
    {
        //Arrange
        Order order = new Order();

        BigDecimal price = new BigDecimal(39.99);

        Book book = new Book();
        book.setAmount(0);
        book.setPrice(price);

        Book orderedBook = new Book();
        orderedBook.setAmount(2);

        order.getBooks().add(orderedBook);

        Mockito.when(em.find(Book.class, orderedBook.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);
        //Assert - spodziewany wyjatek
    }

    @Test(expected = OutOfStockException.class)
    public void whenOrderTotalPriceIsLowerThanMinimumTotalPrice_placeOrderThrowsEx()
    {
        Order order = new Order();

        Book book = new Book();
        book.setAmount(3);

        Book orderedBook = new Book();
        orderedBook.setAmount(1);


        Mockito.when(em.find(Book.class, orderedBook.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);
        BigDecimal lessThenMinimumPrice = ordersService.getMinimalTotalPrice().subtract(new BigDecimal(1));

        book.setPrice(lessThenMinimumPrice);
        order.getBooks().add(orderedBook);

        //Act
        ordersService.placeOrder(order);
        //Assert - spodziewany wyjatek
    }

    @Test
    public void whenOrderTotalPriceEqualsMinimumTotalPrice_placeOrderSucceeds()
    {
        Order order = new Order();

        Book book = new Book();
        book.setAmount(3);

        Book orderedBook = new Book();
        orderedBook.setAmount(1);


        Mockito.when(em.find(Book.class, orderedBook.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);
        BigDecimal minimumPrice = ordersService.getMinimalTotalPrice();

        book.setPrice(minimumPrice);
        order.getBooks().add(orderedBook);

        //Act
        ordersService.placeOrder(order);
        //Assert
        assertEquals(2, (int)book.getAmount());
    }

    @Test(expected = OutOfStockException.class)
    public void whenOrderContainsDuplicates_placeOrderThrowsEx()
    {
        //Arrange
        Order order = new Order();

        BigDecimal price = new BigDecimal(39.99);

        Book book = new Book();
        book.setAmount(4);
        book.setPrice(price);

        Book orderedBook1 = new Book();
        orderedBook1.setAmount(1);

        order.getBooks().add(orderedBook1);
        order.getBooks().add(orderedBook1);


        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);
        //Assert - spodziewany wyjatek
    }

    @Test(expected = OutOfStockException.class)
    public void whenOrderedBookAmountEquals0_placeOrderThrowsEx()
    {
        Order order = new Order();

        BigDecimal price = new BigDecimal(39.99);

        Book book = new Book();
        book.setAmount(3);
        book.setPrice(price);

        Book orderedBook = new Book();
        orderedBook.setAmount(0);

        order.getBooks().add(orderedBook);

        Mockito.when(em.find(Book.class, orderedBook.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - spodziewany wyjatek
    }
}