package net.stawrul.controllers;

import net.stawrul.model.Book;
import net.stawrul.services.BooksService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.CONFLICT;


/**
 * Kontroler zawierający akcje związane z książkami w sklepie.
 * <p>
 * Parametr "/books" w adnotacji @RequestMapping określa prefix dla adresów wszystkich akcji kontrolera.
 */
@RestController
@RequestMapping("/books")
public class BooksController
{

    final BooksService booksService;

    public BooksController(BooksService booksService)
    {
        this.booksService = booksService;
    }


    @GetMapping
    public List<Book> listBooks()
    {
        return booksService.findAll();
    }


    @PostMapping
    public ResponseEntity<Void> addBook(@RequestBody Book book, UriComponentsBuilder uriBuilder)
    {

        if (booksService.find(book.getId()) == null)
        {
            booksService.save(book);

            //Jeśli zapisywanie się powiodło zwracana jest odpowiedź 201 Created z nagłówkiem Location, który zawiera
            //adres nowo dodanej książki
            URI location = uriBuilder.path("/books/{id}").buildAndExpand(book.getId()).toUri();
            return ResponseEntity.created(location).build();

        }
        else
        {
            //Identyfikator książki już istnieje w bazie danych. Żądanie POST służy do dodawania nowych elementów,
            //więc zwracana jest odpowiedź z kodem błędu 409 Conflict
            return ResponseEntity.status(CONFLICT).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Book> getBook(@PathVariable UUID id)
    {
        Book book = booksService.find(id);

        //W warstwie biznesowej brak książki o podanym id jest sygnalizowany wartością null. Jeśli książka nie została
        //znaleziona zwracana jest odpowiedź 404 Not Found. W przeciwnym razie klient otrzymuje odpowiedź 200 OK
        //zawierającą dane książki w domyślnym formacie JSON
        return book != null ? ResponseEntity.ok(book) : ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateBook(@RequestBody Book book)
    {
        if (booksService.find(book.getId()) != null)
        {
            booksService.save(book);
            return ResponseEntity.ok().build();

        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }
}
