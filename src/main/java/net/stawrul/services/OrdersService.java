package net.stawrul.services;

import net.stawrul.model.Book;
import net.stawrul.model.Order;
import net.stawrul.services.exceptions.OutOfStockException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

@Service
public class OrdersService extends EntityService<Order>
{
    private final BigDecimal minimalTotalPrice = new BigDecimal(29.99);

    public OrdersService(EntityManager em)
    {
        super(em, Order.class, Order::getId);
    }

    public List<Order> findAll()
    {
        return em.createQuery("SELECT o FROM Order o", Order.class)
                 .getResultList();
    }

    public BigDecimal getMinimalTotalPrice()
    {
        return minimalTotalPrice;
    }

    @Transactional
    public void placeOrder(Order order)
    {
        //Sprawdź duplikaty, ilosc zamawianych towarow
        order.getBooks()
             .stream()
             .sorted((b1, b2) -> {
                 if (b1.getId()
                       .equals(b2.getId()))
                     throw new OutOfStockException();
                 return b1.getAmount()
                          .compareTo(b2.getAmount());
             })
             .forEach((b) -> {
                 Book book = em.find(Book.class, b.getId());
                 if (b.getAmount() < 1)
                     throw new OutOfStockException();
                 if (book.getAmount() < b.getAmount())
                     throw new OutOfStockException();
             });

        //Sprawdz posiada minimalna wartosc jaka sklep realizuje
        Boolean condition = order.getBooks()
                             .stream()
                             .map((b) -> {
                                 Book book = em.find(Book.class, b.getId());
                                 return book.getPrice()
                                            .multiply(new BigDecimal(b.getAmount()));
                             })
                             .reduce(BigDecimal.ZERO, BigDecimal::add)
                             .compareTo(minimalTotalPrice) == -1;
        if (condition)
            throw new OutOfStockException();

        //Nie rzuciło wyjątku - więc można składać zamówienie
        order.getBooks()
             .forEach((b) -> {
                 Book book = em.find(Book.class, b.getId());
                 int newAmount = book.getAmount() - b.getAmount();
                 book.setAmount(newAmount);
             });

        save(order);
    }
}
