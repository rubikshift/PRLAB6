package net.stawrul.services;

import net.stawrul.model.Book;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Komponent (serwis) biznesowy do realizacji operacji na książkach.
 */
@Service
public class BooksService extends EntityService<Book>
{
    public BooksService(EntityManager em)
    {
        super(em, Book.class, Book::getId);
    }

    public List<Book> findAll()
    {
        return em.createNamedQuery(Book.FIND_ALL, Book.class).getResultList();
    }

}
